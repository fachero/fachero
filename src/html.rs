macro_rules! html {
    {$($el: ident $($val: block)?)+} => {
        $(
            let value: Option<usize> = None;
            $(let value = $val)?
            println!("{:?}", value);
        )+
    };
}

#[cfg(test)]
mod tests {
    #[test]
    fn test() {
        html! {
            h1 {6}
            h2
            h3 {5}
        }
    }
}